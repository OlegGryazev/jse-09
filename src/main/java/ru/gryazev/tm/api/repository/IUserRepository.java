package ru.gryazev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    public List<User> findAll();

    @Nullable
    public User findUserByIndex(int userIndex);

}
