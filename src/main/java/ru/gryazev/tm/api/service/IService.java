package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.repository.IRepository;
import ru.gryazev.tm.entity.AbstractCrudEntity;

import java.util.List;

public interface IService<T extends AbstractCrudEntity> {

    @Nullable
    public T create(@Nullable String userId, @Nullable T t);

    @Nullable
    public T findOne(@Nullable String userId, @Nullable String entityId);

    @NotNull
    public List<T> findByUserId(@Nullable String userId);

    @Nullable
    public T edit(@Nullable String userId, @Nullable T t);

    @Nullable
    public T remove(@Nullable String userId, @Nullable String entityId);

    public void clear(@Nullable String userId);

    @NotNull
    public IRepository<T> getRepository();

}
