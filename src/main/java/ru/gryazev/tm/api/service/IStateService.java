package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.entity.ComparableEntity;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Project;

import java.util.Comparator;
import java.util.Map;

public interface IStateService {

    public boolean isUserLogged();

    @Nullable
    public String getCurrentUserId();

    @Nullable
    public String getCurrentProjectId();

    @NotNull
    public Map<String, AbstractCommand> getCommands();

    public void setCurrentUserId(@Nullable String currentUserId);

    public void setCurrentProjectId(@Nullable String currentProjectId);

    public void setCommands(@NotNull Map<String, AbstractCommand> commands);

    @NotNull
    public Comparator<ComparableEntity> getProjectComparator();

    public void setProjectComparator(@NotNull Comparator<ComparableEntity> projectComparator);

    @NotNull
    public Comparator<ComparableEntity> getTaskComparator();

    public void setTaskComparator(@NotNull Comparator<ComparableEntity> taskComparator);

}
