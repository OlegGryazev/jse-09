package ru.gryazev.tm.command.user;

import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.error.CrudCreateException;
import ru.gryazev.tm.error.CrudUpdateException;

import java.io.IOException;

public final class UserRegistrationCommand extends AbstractCommand {

    public UserRegistrationCommand() {
        setAllowed(true);
    }

    @Override
    public String getName() {
        return "user-registration";
    }

    @Override
    public String getDescription() {
        return "Registration of new user.";
    }

    @Override
    public void execute() throws IOException {
        if (serviceLocator == null || terminalService == null) return;
        @Nullable final User user = terminalService.getUserPwdRepeat();
        if (user == null) throw new CrudUpdateException();

        @Nullable final User createdUser = serviceLocator.getUserService().create(user.getUserId(), user);
        if (createdUser == null) throw new CrudCreateException();
        terminalService.print("[OK]");
    }

}
