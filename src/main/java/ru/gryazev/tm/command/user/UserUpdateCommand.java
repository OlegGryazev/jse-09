package ru.gryazev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.error.CrudUpdateException;

import java.io.IOException;

public final class UserUpdateCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "user-update";
    }

    @Override
    public String getDescription() {
        return "Update user password.";
    }

    @Override
    public void execute() throws IOException {
        if (serviceLocator == null || terminalService == null || stateService == null) return;
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @Nullable final String pwd = terminalService.getPwdHashFromConsole();
        @Nullable final String pwdRepeat = terminalService.getPwdHashFromConsole();
        if (pwd == null || !pwd.equals(pwdRepeat)){
            terminalService.print("Entered passwords do not match!");
            return;
        }
        @Nullable final String currentUserId = stateService.getCurrentUserId();

        @Nullable final User user = userService.findOne(currentUserId, currentUserId);
        if (user == null) throw new CrudNotFoundException();
        user.setPwdHash(pwd);
        @Nullable final User editedUser = userService.edit(currentUserId, user);
        if (editedUser == null) throw new CrudUpdateException();
        terminalService.print("[OK]");
    }

}
