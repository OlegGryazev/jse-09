package ru.gryazev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.error.CrudListEmptyException;

import java.io.IOException;
import java.util.List;

public class ProjectFindByName extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-find-name";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "List of projects by name or part of name.";
    }

    @Override
    public void execute() throws IOException {
        if (serviceLocator == null || terminalService == null || stateService == null) return;
        @NotNull final String projectName = terminalService.getSearchString();
        @NotNull final List<Project> projects = serviceLocator.getProjectService()
                .findByName(stateService.getCurrentUserId(),projectName);
        if (projects.size() == 0) throw new CrudListEmptyException();
        projects.sort(stateService.getProjectComparator());
        for (int i = 0; i < projects.size(); i++)
            terminalService.print((i + 1) + ". " + projects.get(i).getName());
    }

}
