package ru.gryazev.tm.command.project.sort;

import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.comparator.ComparableEntityComparator;

public class ProjectSortStatus extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-sort-status";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Sorts projects in order of project status.";
    }

    @Override
    public void execute() {
        if (terminalService == null || stateService == null) return;
        stateService.setProjectComparator(ComparableEntityComparator.comparatorStatus);
        terminalService.print("[OK]");
    }

}
