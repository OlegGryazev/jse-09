package ru.gryazev.tm.command.project.sort;

import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.comparator.ComparableEntityComparator;

public class ProjectSortDefault extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-sort-default";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Sorts projects in order of creation.";
    }

    @Override
    public void execute() {
        if (terminalService == null || stateService == null) return;
        stateService.setProjectComparator(ComparableEntityComparator.comparatorCreateMillis);
        terminalService.print("[OK]");
    }

}
