package ru.gryazev.tm.command.project.sort;

import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.comparator.ComparableEntityComparator;

public class ProjectSortDateStart extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-sort-date-start";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Sorts projects in order of date of project start.";
    }

    @Override
    public void execute() {
        if (terminalService == null || stateService == null) return;
        stateService.setProjectComparator(ComparableEntityComparator.comparatorDateStart);
        terminalService.print("[OK]");
    }

}
