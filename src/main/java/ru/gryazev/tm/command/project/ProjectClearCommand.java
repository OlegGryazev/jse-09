package ru.gryazev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;

public final class ProjectClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Clear projects list.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null || stateService == null || terminalService == null) return;
        @Nullable final String userId = stateService.getCurrentUserId();
        serviceLocator.getProjectService().clear(userId);
        serviceLocator.getTaskService().clear(userId);
        terminalService.print("[CLEAR OK]");
    }

}
