package ru.gryazev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.enumerated.Status;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.error.CrudUpdateException;

import java.io.IOException;

public class ProjectSetStatus extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-set-status";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Set entered status to project.";
    }

    @Override
    public void execute() throws IOException {
        if (serviceLocator == null || stateService == null || terminalService == null) return;
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @Nullable final String userId = stateService.getCurrentUserId();
        final int projectIndex = terminalService.getProjectIndex();
        @Nullable final String projectId = projectService.getProjectId(projectIndex, stateService.getCurrentUserId(), stateService.getProjectComparator());
        if (projectId == null) throw new CrudNotFoundException();

        @Nullable final Project project = projectService.findOne(userId, projectId);
        if (project == null) throw new CrudNotFoundException();
        @Nullable final Status status = terminalService.getStatus();
        if (status == null) throw new CrudUpdateException();
        project.setStatus(status);
        @Nullable final Project editedProject = projectService.edit(userId, project);
        if (editedProject == null) throw new CrudUpdateException();
        terminalService.print("[OK]");
    }

}
