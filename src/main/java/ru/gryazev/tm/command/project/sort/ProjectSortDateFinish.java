package ru.gryazev.tm.command.project.sort;

import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.comparator.ComparableEntityComparator;

public class ProjectSortDateFinish extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-sort-date-finish";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Sorts projects in order of date of project finish.";
    }

    @Override
    public void execute() {
        if (terminalService == null || stateService == null) return;
        stateService.setProjectComparator(ComparableEntityComparator.comparatorDateFinish);
        terminalService.print("[OK]");
    }

}
