package ru.gryazev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.error.CrudListEmptyException;

import java.io.IOException;
import java.util.List;

public class ProjectFindByDetails extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-find-details";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "List of projects by details or part of details.";
    }

    @Override
    public void execute() throws IOException {
        if (serviceLocator == null || terminalService == null || stateService == null) return;
        @NotNull final String projectDetails = terminalService.getSearchString();
        @NotNull final List<Project> projects = serviceLocator.getProjectService()
                .findByDetails(stateService.getCurrentUserId(),projectDetails);
        if (projects.size() == 0) throw new CrudListEmptyException();
        projects.sort(stateService.getProjectComparator());
        for (int i = 0; i < projects.size(); i++)
            terminalService.print((i + 1) + ". " + projects.get(i).getName());
    }

}
