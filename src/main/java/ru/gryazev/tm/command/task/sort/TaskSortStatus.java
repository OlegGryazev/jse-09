package ru.gryazev.tm.command.task.sort;

import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.comparator.ComparableEntityComparator;

public class TaskSortStatus extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-sort-status";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Sorts tasks in order of task status.";
    }

    @Override
    public void execute() {
        if (terminalService == null || stateService == null) return;
        stateService.setTaskComparator(ComparableEntityComparator.comparatorStatus);
        terminalService.print("[OK]");
    }

}
