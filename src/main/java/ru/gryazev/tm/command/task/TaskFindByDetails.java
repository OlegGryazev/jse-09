package ru.gryazev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.error.CrudListEmptyException;

import java.io.IOException;
import java.util.List;

public class TaskFindByDetails extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-find-details";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "List of tasks by details or part of details.";
    }

    @Override
    public void execute() throws IOException {
        if (serviceLocator == null || terminalService == null || stateService == null) return;
        @Nullable final String userId = stateService.getCurrentUserId();
        @NotNull final String taskDetails = terminalService.getSearchString();
        @NotNull final List<Task> tasks = serviceLocator.getTaskService()
                .findByDetails(userId,taskDetails);
        if (tasks.size() == 0) throw new CrudListEmptyException();
        tasks.sort(stateService.getTaskComparator());
        for (int i = 0; i < tasks.size(); i++) {
            @Nullable final Project project = serviceLocator.getProjectService().findOne(userId, tasks.get(i).getProjectId());
            @NotNull final String linkedTo = (project == null) ?
                    "unlinked" : "linked to project " + project.getName();
            terminalService.print((i + 1) + ". " + tasks.get(i).getName() + " : " + linkedTo);
        }
    }

}
