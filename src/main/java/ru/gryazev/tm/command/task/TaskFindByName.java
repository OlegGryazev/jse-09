package ru.gryazev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.error.CrudListEmptyException;

import java.io.IOException;
import java.util.List;

public class TaskFindByName extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-find-name";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "List of projects by name or part of name.";
    }

    @Override
    public void execute() throws IOException {
        if (serviceLocator == null || terminalService == null || stateService == null) return;
        @Nullable final String userId = stateService.getCurrentUserId();
        @NotNull final String taskName = terminalService.getSearchString();
        @NotNull final List<Task> tasks = serviceLocator.getTaskService()
                .findByName(userId,taskName);
        if (tasks.size() == 0) throw new CrudListEmptyException();
        tasks.sort(stateService.getTaskComparator());
        for (int i = 0; i < tasks.size(); i++) {
            @Nullable final Project project = serviceLocator.getProjectService().findOne(userId, tasks.get(i).getProjectId());
            @NotNull final String linkedTo = (project == null) ?
                    "unlinked" : "linked to project " + project.getName();
            terminalService.print((i + 1) + ". " + tasks.get(i).getName() + " : " + linkedTo);
        }
    }

}
