package ru.gryazev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.enumerated.Status;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.error.CrudUpdateException;

import java.io.IOException;

public class TaskSetStatus extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-set-status";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Set entered status to task.";
    }

    @Override
    public void execute() throws IOException {
        if (serviceLocator == null || terminalService == null || stateService == null) return;
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @Nullable final String userId = stateService.getCurrentUserId();
        @Nullable String currentProjectId = stateService.getCurrentProjectId();
        if (currentProjectId == null) currentProjectId = serviceLocator.getProjectService()
                .getProjectId(terminalService.getProjectIndex(), userId, stateService.getProjectComparator());
        if (currentProjectId == null) throw new CrudNotFoundException();
        final int taskIndex = terminalService.getTaskIndex();
        @Nullable final String taskId = taskService.getTaskId(currentProjectId, userId, taskIndex);
        if (taskId == null) throw new CrudNotFoundException();

        @Nullable final Task task = taskService.findOne(userId, taskId);
        if (task == null) throw new CrudNotFoundException();
        @Nullable final Status status = terminalService.getStatus();
        if (status == null) throw new CrudUpdateException();
        task.setStatus(status);
        @Nullable final Task editedTask = taskService.edit(userId, task);
        if (editedTask == null) throw new CrudUpdateException();
        terminalService.print("[OK]");
    }

}
