package ru.gryazev.tm.command.task.sort;

import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.comparator.ComparableEntityComparator;

public class TaskSortDateStart extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-sort-date-start";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Sorts tasks in order of date of task start.";
    }

    @Override
    public void execute() {
        if (terminalService == null || stateService == null) return;
        stateService.setTaskComparator(ComparableEntityComparator.comparatorDateStart);
        terminalService.print("[OK]");
    }

}
