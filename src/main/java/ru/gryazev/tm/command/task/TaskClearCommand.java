package ru.gryazev.tm.command.task;

import ru.gryazev.tm.command.AbstractCommand;

public final class TaskClearCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public String getDescription() {
        return "Clear tasks list.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null || terminalService == null || stateService == null) return;
        serviceLocator.getTaskService().clear(stateService.getCurrentUserId());
        terminalService.print("[CLEAR OK]");
    }

}
