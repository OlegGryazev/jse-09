package ru.gryazev.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.entity.ComparableEntity;
import ru.gryazev.tm.api.service.IStateService;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.comparator.ComparableEntityComparator;
import ru.gryazev.tm.entity.Project;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;

@Setter
@Getter
public final class StateService implements IStateService {

    @Nullable
    private String currentUserId = null;

    @Nullable
    private String currentProjectId = null;

    @NotNull
    private Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    private Comparator<ComparableEntity> projectComparator = ComparableEntityComparator.comparatorCreateMillis;

    @NotNull
    private Comparator<ComparableEntity> taskComparator = ComparableEntityComparator.comparatorCreateMillis;

    public boolean isUserLogged(){
        return currentUserId != null;
    }

}
