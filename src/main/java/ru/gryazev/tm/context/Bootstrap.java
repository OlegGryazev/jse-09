package ru.gryazev.tm.context;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.api.repository.IProjectRepository;
import ru.gryazev.tm.api.repository.ITaskRepository;
import ru.gryazev.tm.api.repository.IUserRepository;
import ru.gryazev.tm.api.service.*;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.enumerated.RoleType;
import ru.gryazev.tm.error.CrudException;
import ru.gryazev.tm.repository.ProjectRepository;
import ru.gryazev.tm.repository.TaskRepository;
import ru.gryazev.tm.repository.UserRepository;
import ru.gryazev.tm.service.*;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final ITerminalService terminalService = new TerminalService();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @Getter
    @NotNull
    private final IStateService stateService = new StateService();

    public void init(@NotNull final Set<Class<? extends AbstractCommand>> commandClasses) {
        terminalService.print("**** Welcome to Project Manager ****");
        commandsInit(commandClasses);
        usersInit();
        while (true) {
            try {
                @Nullable final AbstractCommand command = getCommand();
                if (!checkCommandAllowed(command)) continue;
                command.execute();
            } catch (CrudException | IOException e) {
                terminalService.print(e.getMessage());
            }
        }
    }

    private void commandsInit(@NotNull final Set<Class<? extends AbstractCommand>> commandClasses) {
        @NotNull final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
        for (@NotNull final Class<? extends AbstractCommand> clazz : commandClasses){
            @Nullable AbstractCommand command = null;
            try {
                command = (AbstractCommand) clazz.newInstance();;
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
            if (command == null) continue;
            command.setServiceLocator(this);
            command.setTerminalService(terminalService);
            command.setStateService(stateService);
            commands.put(command.getName(), command);
        }
        stateService.setCommands(commands);
    }

    private void usersInit() {
        @NotNull final User user = new User();
        user.setLogin("user");
        user.setPwdHash("c4ca4238a0b923820dcc509a6f75849b");
        user.setRoleType(RoleType.USER);
        userService.create(user.getId(), user);

        @NotNull final User admin = new User();
        admin.setLogin("admin");
        admin.setPwdHash("c4ca4238a0b923820dcc509a6f75849b");
        admin.setRoleType(RoleType.ADMIN);
        userService.create(admin.getId(), admin);
    }

    @Nullable
    private AbstractCommand getCommand() throws IOException {
        @Nullable final AbstractCommand command = stateService.getCommands().get(terminalService.readCommand());
        if (command == null) {
            terminalService.print("Command not found!");
            return null;
        }
        return command;
    }

    private boolean checkCommandAllowed(AbstractCommand command) {
        if (command == null) return false;
        @Nullable final String currentUserId = stateService.getCurrentUserId();
        @Nullable final RoleType[] roles = command.getRoles();
        final boolean checkAccess = stateService.isUserLogged() || command.isAllowed();
        final boolean checkRole = command.getRoles() == null || userService.checkRole(currentUserId, roles);
        if (!checkAccess || !checkRole) {
            terminalService.print("Access denied!");
            return false;
        }
        return true;
    }

}
