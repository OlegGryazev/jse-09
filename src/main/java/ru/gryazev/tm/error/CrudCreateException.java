package ru.gryazev.tm.error;

public final class CrudCreateException extends CrudException {

    public CrudCreateException() {
        super("Error during create operation");
    }

}
