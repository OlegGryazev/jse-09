package ru.gryazev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.entity.ComparableEntity;
import ru.gryazev.tm.enumerated.Status;
import ru.gryazev.tm.util.DateUtils;

import java.util.Date;

@Getter
@Setter
public final class Task extends AbstractCrudEntity implements ComparableEntity {

    @Nullable
    private String projectId = null;

    @Nullable
    private String name = "";

    @Nullable
    private String details = "";

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    @NotNull
    private Status status = Status.PLANNED;

    private final Long createMillis = new Date().getTime();

    @NotNull
    @Override
    public String toString() {
        return String.format("Name: %s\n" +
                        "Details: %s\n" +
                        "Starts: %s\n" +
                        "Ends: %s\n" +
                        "Status: %s",
                name,
                details,
                DateUtils.formatDateToString(dateStart),
                DateUtils.formatDateToString(dateFinish),
                status.displayName());
    }

}
